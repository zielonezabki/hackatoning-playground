var calc = new Vue({
    el: '.INGcalc',
    data: {
        amount: 1000,
        period: 3,
        periodLabel: 'm-ce'
    },
    methods: {

    }
});

calc.$watch('period', function (newVal) {
    if (newVal % 10 == 2 || newVal % 10 == 3 || newVal % 10 == 4) {
        calc.$data.periodLabel = 'm-ce';
    } else {
        calc.$data.periodLabel = 'm-cy';
    }
});
